---
order: 0
title:
  zh-CN: 基本
  en-US: Basic
---

[Demo Source Code](https://github.com/ant-design/ant-design-mobile-rn/blob/master/components/textarea-item/demo/basic.tsx)

```jsx
/* tslint:disable:no-console */
import { MoMoTextInput } from 'momo-components';
import React from 'react';
import { View } from 'react-native';

export default class MoMoTextInputExample extends React.Component {
  constructor(props) {
    super(props);  
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <MoMoTextInput/>  
      </View>
    );
  }
}
```
