/* tslint:disable:no-console */
import React from 'react';
import { View } from 'react-native';
import { MoMoTextInput } from '../../';

export default class MoMoTextInputExample extends React.Component {
  constructor(props: any) {
    super(props);
  }
  
  render() {
    return (
      <View style={{ flex: 1 }}>
        <MoMoTextInput/>  
      </View>
    );
  }
}
